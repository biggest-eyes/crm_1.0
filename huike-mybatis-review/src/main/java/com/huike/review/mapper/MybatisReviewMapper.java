package com.huike.review.mapper;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {


    /**======================================================新增======================================================**/

    public void save(Review review);

    /**======================================================删除======================================================**/

    public void delete(Long id);

    /**======================================================修改======================================================**/

    public void update(Review review);

    /**======================================================简单查询===================================================**/

    public Review getById(Long id);

    public List<Review> getDataByPage(@Param("start") Integer start,@Param("end") Integer end);

    public long getCount();

}
