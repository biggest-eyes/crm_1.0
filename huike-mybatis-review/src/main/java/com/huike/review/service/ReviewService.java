package com.huike.review.service;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {

    public void save(Review review);

    public void delete(Long id);

    public void update(Review review);

    public Review getById(Long id);

    public List<Review> getDataByPage(Integer currentPage, Integer pageSize);

    public long getCount();
}
