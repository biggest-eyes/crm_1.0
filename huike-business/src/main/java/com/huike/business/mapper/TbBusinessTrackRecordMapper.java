package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
public interface TbBusinessTrackRecordMapper {
    @Select("SELECT * FROM tb_business_track_record WHERE business_id=#{businessId}")
    List<TbBusinessTrackRecord> getRecordServiceByBusinessId(@PathVariable("businessId") Long id);
    @Insert("INSERT INTO tb_business_track_record (business_id,create_by,key_items,record,create_time,track_status,next_time) " +
            "VALUES (#{tbBusinessTrackRecord.businessId},#{tbBusinessTrackRecord.createBy},#{tbBusinessTrackRecord.keyItems},#{tbBusinessTrackRecord.record},#{tbBusinessTrackRecord.createTime},#{tbBusinessTrackRecord.trackStatus},#{tbBusinessTrackRecord.nextTime})")
    void save(@Param("tbBusinessTrackRecord") TbBusinessTrackRecord tbBusinessTrackRecord);
}