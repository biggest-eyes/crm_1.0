package com.huike.business.service.impl;


import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.common.utils.SecurityUtils;
import com.sun.prism.impl.BaseContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商机跟进记录Service业务层处理
 *
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {
    @Autowired
    private TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

    @Autowired
    private TbBusinessMapper businessMapper;
    @Override
    public List<TbBusinessTrackRecord> getRecordServiceByBusinessId(Long id) {
        return tbBusinessTrackRecordMapper.getRecordServiceByBusinessId(id);

    }

    @Override
    public void saveBusinessAndTrackRecord(BusinessTrackVo businessTrackVo) {
        TbBusiness tbBusiness = new TbBusiness();
        TbBusinessTrackRecord tbBusinessTrackRecord = new TbBusinessTrackRecord();
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        BeanUtils.copyProperties(businessTrackVo,tbBusinessTrackRecord);
        tbBusiness.setId(businessTrackVo.getBusinessId());
        tbBusinessTrackRecord.setBusinessId(businessTrackVo.getBusinessId());
        tbBusinessTrackRecord.setCreateBy(SecurityUtils.getUsername());
        tbBusiness.setCreateBy(SecurityUtils.getUsername());
        System.out.println(tbBusiness);
        System.out.println(tbBusinessTrackRecord);
        businessMapper.updateTbBusiness(tbBusiness);
        tbBusinessTrackRecordMapper.save(tbBusinessTrackRecord);

    }
}
