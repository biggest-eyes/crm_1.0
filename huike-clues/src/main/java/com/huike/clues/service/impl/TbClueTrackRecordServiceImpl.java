package com.huike.clues.service.impl;


import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 线索跟进记录Service业务层处理
 *
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Autowired
    private TbClueMapper tbClueMapper;



    @Override
    public Integer add(ClueTrackRecordVo tbClueTrackRecord) {
        String username = SecurityUtils.getUsername();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String today = sdf.format(date);
        tbClueTrackRecord.setCreateBy(username);
        tbClueTrackRecord.setCreateTime(today);
        int add = tbClueTrackRecordMapper.add(tbClueTrackRecord);
        tbClueMapper.add(tbClueTrackRecord);
        return add;
    }

    @Override
    public List<ClueTrackRecordVo> list(Long clueId) {
        List<ClueTrackRecordVo> list = tbClueTrackRecordMapper.list(clueId);
        return list;
    }
}
