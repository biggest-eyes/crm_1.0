package com.huike.web.controller.review;


import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;


    /**
     * =========================================================新增数据============================================
     */

    @PostMapping("/saveData")
    public AjaxResult save(@RequestBody Review review, HttpSession session) {
        reviewService.save(review);
        return AjaxResult.success("成功插入一条数据");
    }

    @GetMapping("/saveData/**")
    public AjaxResult insert(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        String[] split = servletPath.split("/");
        String name = split[3];
        String age = split[4];
        String sex = split[5];
        Review review = new Review();
        review.setName(name);
        review.setAge(Integer.parseInt(age));
        review.setSex(sex);
        reviewService.save(review);
        return AjaxResult.success("成功插入一条数据");
    }

    /**
     * =========================================================删除数据=============================================
     */

    @DeleteMapping("/remove/{id}")
    public AjaxResult delete(@PathVariable("id") Long id) {
        reviewService.delete(id);
        return AjaxResult.success("成功删除:1条数据");
    }

    /**
     * =========================================================修改数据=============================================
     */

    @PostMapping("/update")
    public AjaxResult update(@RequestBody Review review) {
        reviewService.update(review);
        return AjaxResult.success("成功修改:1条数据");
    }

    /**
     * =========================================================查询数据=============================================
     */

    @GetMapping("/getById")
    public AjaxResult getById(Long id) {
        Review review = reviewService.getById(id);
        return AjaxResult.success(review);
    }

    @GetMapping("/getDataByPage")
    public TableDataInfo getDataByPage(Integer pageNum, Integer pageSize) {
        List<Review> reviewList = reviewService.getDataByPage(pageNum, pageSize);
        int count = (int) reviewService.getCount();
        TableDataInfo tableDataInfo = new TableDataInfo(reviewList, count);
        return tableDataInfo;
    }

}