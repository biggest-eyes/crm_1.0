package com.huike.report.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 *
 * @author Administrator
 */
public interface ReportMapper {
    /**=========================================基本数据========================================*/
    /**
     * 获取线索数量
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getCluesNum(@Param("startTime") String beginCreateTime,
                        @Param("endTime") String endCreateTime,
                        @Param("username") String username);

    /**
     * 获取商机数量
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getBusinessNum(@Param("startTime") String beginCreateTime,
                           @Param("endTime") String endCreateTime,
                           @Param("username") String username);

    /**
     * 获取合同数量
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getContractNum(@Param("startTime") String beginCreateTime,
                           @Param("endTime") String endCreateTime,
                           @Param("username") String username);

    /**
     * 获取合同金额
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Double getSalesAmount(@Param("startTime") String beginCreateTime,
                          @Param("endTime") String endCreateTime,
                          @Param("username") String username);

    /**=========================================今日简报========================================*/

    /**
     * 今日新增线索数量
     *
     * @param username 用户名
     * @return
     */
    Integer getTodayCluesNum(@Param("username") String username,
                             @Param("today") String today);

    /**
     * 今日新增商机数量
     *
     * @param username 用户名
     * @return
     */
    Integer getTodayBusinessNum(@Param("username") String username,
                                @Param("today") String today);

    /**
     * 今日新增客户数量
     *
     * @param username 用户名
     * @return
     */
    Integer getTodayContractNum(@Param("username") String username,
                                @Param("today") String today);

    /**
     * 今日新增销售额
     *
     * @param username 用户名
     * @return
     */
    Double getTodaySalesAmount(@Param("username") String username,
                               @Param("today") String today);


    /**=========================================待办========================================*/

    /**
     * 待跟进线索
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getToFollowedCluesNum(@Param("startTime") String beginCreateTime,
                                  @Param("endTime") String endCreateTime,
                                  @Param("username") String username);

    /**
     * 待跟进商机
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getToFollowedBusinessNum(@Param("startTime") String beginCreateTime,
                                     @Param("endTime") String endCreateTime,
                                     @Param("username") String username);


    /**
     * 待分配线索
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getToAllocatedCluesNum(@Param("startTime") String beginCreateTime,
                                   @Param("endTime") String endCreateTime,
                                   @Param("username") String username);

    /**
     * 待分配商机
     *
     * @param beginCreateTime 开始时间
     * @param endCreateTime   结束时间
     * @param username        用户名
     * @return
     */
    Integer getToAllocatedBusinessNum(@Param("startTime") String beginCreateTime,
                                      @Param("endTime") String endCreateTime,
                                      @Param("username") String username);

}
